from xmlrpc.client import boolean
import pydicom as pd
import argparse
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
from helpers import *
import code
import readline                                              
import rlcompleter
from scipy.spatial import Delaunay
import time

def main():
	args = parser.parse_args()
	#ctdir_path			= args.i[0]
	mc_rtdoses_doses		= args.dose_rtdose
	#plan_rtdoses_doses  		= args.c
	mc_rtdoses_dlets		= args.let_rtdose
	#rtplans			= args.p
	rtstruct			= args.structures[0]
	alphabeta           		= args.alphabeta
	outfile 			= args.output_file
	disregard_body    		= args.disregard_body
	physical_dose			= args.physical_dose
	interact 			= args.interactive
	outdir				= args.output_dir
	fractions 			= int(args.fractions[0])
	rbemodel 			= args.model[0]

	print("Loading CTs ...")
	#cts = load_dcmdir(ctdir_path,"ct")
	print("Loading MC doses ...")
	mc_doses = load_dcmlist(mc_rtdoses_doses, "rtdose")
	print("Loading MC LET ...")
	mc_let = load_dcmlist(mc_rtdoses_dlets, "rtdose")
	print("Loading structures ...")
	struct = load_dcmlist([rtstruct],"rtstruct")[0]
	a_b_dict = {}
	print("Loading alpha/beta ratios ...")
	if not alphabeta == None:
		a_b_dict = read_alphabeta_dict(alphabeta[0])
	else:
		a_b_dict = prompt_alphabeta_dict(struct, disrgd_bdy = disregard_body)
		
	

	print("Summing doses ...")
	sum_dose = sum_doses(mc_doses)

	# now go through every point in the rtdose structures and make a total dose averaged LET grid.
	print("Calculating the dose averaged LET for the given LET and dose fields  ...")


	### THIS FUNCTION JUST MAKES A MESS!!
	### FIX IT
	### ARRRHRHRAHRARGAR
	### NOOOOOW
	### IT JUST SEEEEEMS RANDOMMMM
	### I believe it is fixed now 25/3 - RK

	dose_averaged_let = make_dose_averaged_let(mc_doses, mc_let, interactive = interact)  	
		
	coord_grid = make_coord_array(dose_averaged_let)
	alphaBetaGrid = np.zeros(shape=dose_averaged_let.pixel_array.shape)
	if not disregard_body:
		print("Calculating which voxels are inside the body")
		body_contour = getROIContourSequenceXyz(struct,ROIName2Number(struct,a_b_dict["body_name"]))
		delaunay_obj = Delaunay(body_contour)
		t0 = time.time()
		body_inds = np.where( (delaunay_obj.find_simplex(coord_grid) >= 0) == True)
		t1 = time.time()
		print("It takes {0} s to find all voxels inside the structure {1}.".format(t1-t0,a_b_dict["body_name"]))
	else:
		# We don't need the body contour as all voxels outside will also be set to whatever the body is set to
		# This works because the RBE values outside of the patient is a bit nonsensical really
		body_inds = np.where(coord_grid==coord_grid)[:3]
	

	# Setting alphabetagrid[body] = alphabetavalue[body]
	alphaBetaGrid[body_inds] = a_b_dict["rest_of_body"]
	# now set all other structs given
	for structname in a_b_dict.keys():
		if structname == "rest_of_body" or structname == "body_name":
			continue
		struct_contourXyz = getROIContourSequenceXyz(struct,ROIName2Number(struct,structname))
		struct_delaunay = Delaunay(struct_contourXyz)
		#find all points in coord_grid which are in struct_contourXyz
		structname_inds = np.where( (struct_delaunay.find_simplex(coord_grid) >= 0) == True)
		alphaBetaGrid[structname_inds] = a_b_dict[structname]
	
	# Now we have an alphabeta grid.
	# For each voxel set voxelvalue to be rbe_calculator(dosegrid,letgrid,alphabetagrid,fractionScheme,rbemodel, biological dose given? = True)
	if rbemodel != "all":
		var_rbe_dose, rbegrid 		= rbe_calculator(mc_doses,     			 \
												  mc_let,        			  \
												  alphaBetaGrid, 			  \
												  [fractions]*len(mc_let),    \
												  rbemodel, 				  \
												  biodose=(not physical_dose),\
												  interactivemode = interact)


	# Saving some extra dicoms for testing


	unique_ID = str(np.random.randint(00000,99999))

	outdir = outdir[0] if outdir else "../Extra_info_{0}_RTDOSE_ID_{1}".format(dose_averaged_let.PatientName,unique_ID)
	
	if interact:
		location = "We are in main, line 138"
		vars = globals()       
		vars.update(locals())
		readline.set_completer(rlcompleter.Completer(vars).complete) 
		readline.parse_and_bind("tab: complete")                     
		code.InteractiveConsole(vars).interact()	

	if is_path_exists_or_creatable(outdir):
		if outdir not in os.listdir():
			os.mkdir(outdir)
	else:
		print("Not able to create folder to save output DICOMS")
		exit()
	# alphabeta map
	ab_rtdose = array2RTDOSE(alphaBetaGrid, dose_averaged_let, "Alpha/beta values in units of Gy. ID: {0}".format(unique_ID))
	ab_rtdose.save_as(outdir+"/AlphaBeta.dcm".format(unique_ID))
	# dose averaged LET
	dose_averaged_let.SeriesDescription = "Dose averaged LET for all fields [MeV/mm]. ID: {0}".format(unique_ID)
	dose_averaged_let.save_as(outdir+"/DoseAveragedLET.dcm".format(unique_ID))
	# variable rbe values
	if rbemodel != "all":
		rbe_rtdose = array2RTDOSE(rbegrid, dose_averaged_let, \
			"Variable RBE values using RBE model: {0}. ID: {1}".format(rbemodel,unique_ID))
		rbe_rtdose.save_as(outdir+"/RBE_values_{1}.dcm".format(unique_ID, rbemodel))
	else:
		for model in ["carabe", "wedenberg", "mcnamara", "lwd"]:
			var_rbe_dose, rbegrid 		= rbe_calculator(mc_doses,     			 \
												  mc_let,        			  \
												  alphaBetaGrid, 			  \
												  [fractions]*len(mc_let),    \
												  model, 				  \
												  biodose=(not physical_dose),\
												  interactivemode = False)
			rbe_rtdose = array2RTDOSE(rbegrid, dose_averaged_let, \
			"Variable RBE values using RBE model: {0}. ID: {1}".format(model,unique_ID))
			rbe_rtdose.save_as(outdir+"/RBE_values_{1}.dcm".format(unique_ID, model))

	sum_dose_mult = 1.1 if physical_dose else 1
	sum_rtdose = array2RTDOSE(sum_dose.pixel_array*sum_dose.DoseGridScaling * sum_dose_mult, dose_averaged_let, "Sum of all MC RTDOSE's [Gy(RBE1.1)]. ID: {0}".format(unique_ID))
	sum_rtdose.save_as(outdir+"/SumMCDoses.dcm".format(unique_ID))
	print("The unique ID for this run is: ID_{0}".format(unique_ID))



	if rbemodel != "all":
		if not outfile == None:
			if type(outfile) == list:
				outfile = outfile[0]
			if outfile == "default":
					outfile = outdir+"/var_RBE_{1}.dcm".format(unique_ID, rbemodel)
			elif not is_path_exists_or_creatable(outfile):
				print("The file path you wrote is either not valid or the file already exists.")
				success = False
				while not success:
					outfile = input("Please choose a filename for the result\n>>> ")
					if is_path_exists_or_creatable(outfile):
						success = True
						continue
					print("The file path you wrote is either not valid or the file already exists.")
			var_rbe_dose.save_as(outfile)
		else:
			success = False
			while not success:
				outfile = input("Please choose a filename for the result. Input \'default\' for a generic save in EXTRA_RTDOSE folder.\n>>> ")
				if outfile == "default":
					outfile = outdir+"/var_RBE_{1}.dcm".format(unique_ID, rbemodel)
					success = True
				if is_path_exists_or_creatable(outfile):
					success = True
					continue
				print("The file path you wrote is either not valid or the file already exists.")
			var_rbe_dose.save_as(outfile)
	else: 
		for model in ["carabe", "mcnamara", "wedenberg", "lwd"]:
			var_rbe_dose, rbegrid 		= rbe_calculator(mc_doses,     			 \
												  mc_let,        			  \
												  alphaBetaGrid, 			  \
												  [fractions]*len(mc_let),    \
												  model, 				  \
												  biodose=(not physical_dose),\
												  interactivemode = False)
			if not outfile == None:
				if type(outfile) == list:
					outfile = outfile[0]
				if outfile == "default":
						outfile = outdir+"/var_RBE_{1}.dcm".format(unique_ID, model)
				elif not is_path_exists_or_creatable(outfile):
					print("The file path you wrote is either not valid or the file already exists.")
					success = False
					while not success:
						outfile = input("Please choose a filename for the result\n>>> ")
						if is_path_exists_or_creatable(outfile):
							success = True
							continue
						print("The file path you wrote is either not valid or the file already exists.")
				var_rbe_dose.save_as(outfile)
				outfile = "default"
			else:
				success = False
				while not success:
					outfile = input("Please choose a filename for the result. Input \'default\' for a generic save in EXTRA_RTDOSE folder.\n>>> ")
					if outfile == "default":
						outfile = outdir+"/var_RBE_{1}.dcm".format(unique_ID, model)
						success = True
					if is_path_exists_or_creatable(outfile):
						success = True
						continue
					print("The file path you wrote is either not valid or the file already exists.")
				var_rbe_dose.save_as(outfile)
				outfile = "default"
	
	if interact:
		location = "We are in main, line 168"
		vars = globals()       
		vars.update(locals())
		readline.set_completer(rlcompleter.Completer(vars).complete) 
		readline.parse_and_bind("tab: complete")                     
		code.InteractiveConsole(vars).interact()
	

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Tool for calculating variable RBE models. ')
	# parser.add_argument('-i', type=str, nargs=1, required=True,\
	# 			help='Directory where CT files are located')	
	parser.add_argument('-m','--dose-rtdose', type=str, nargs='+', required=True,\
				help='List of MC RTDOSE dose files')	
	# parser.add_argument('-c', type=str, nargs=1, required=False,\
	# 			help='List of plan RTDOSE dose files. Will make comparison DICOM if present.')
	parser.add_argument('-l','--let-rtdose', type=str, nargs='+', required=True,\
				help='List of MC RTDOSE LET files')
	parser.add_argument('-s','--structures', type=str, nargs=1, required=True,\
				help='Path to RTSTRUCT file')	
	# parser.add_argument('-p', type=str, nargs='+', required=False,\
	# 			help='List of RTPLAN files')
	parser.add_argument('-o','--output-file', type=str, nargs=1, required=False,\
				help='Write the resulting DICOM to this file. Write default to have have default naming.')
	parser.add_argument('-O', '--output-dir', type=str, nargs=1, required=False,\
				help='Write the resulting DICOM files to this directory.\
					 If it does not exist it will be created')
	parser.add_argument('-a','--alphabeta', type=str, nargs=1, required=False,\
				help='Path to file containing alpha/beta ratios for chosen structures. Names must match the structures in the given RTSTRUCT file.')	
	parser.add_argument('--disregard-body', action='store_true', required=False,\
				help='Diseregards the body in terms of alpha beta ratios, as the RBE values are not important outside of\
					  the body anyway.')
	parser.add_argument('--physical-dose', action='store_true', required=False,\
				help='Set this if the MC doses are in physical dose.')
	parser.add_argument('--interactive', action='store_true', required=False,\
				help='Set this if you wish to enter interactive mode in code when done. Can be useful for testing.')
	parser.add_argument('--model', type=str, nargs=1, required=True,\
				help='Which RBE model do you wish to use? You can choose "all" for all models.')	
	parser.add_argument('--fractions', type=str, nargs=1, required=True,\
				help='How many fractions are the treatment designed for?')					
	
				
	args = parser.parse_args()
	main()
