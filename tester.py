from main import main

import os

base_path = "/mnt/Onedrive_AU/Dokumenter/Phd/projects/PBS_prostate_RBE_LET_NTCP/planning_patients/CT_pt18"
ctdir = base_path + "/CTs/"
mcdoses = base_path + "/doses/*.dcm"
plandoses = base_path + "/doses/*.dcm"
mclets = base_path + "/doses/*.dcm"
struct = base_path + "/structures/*.dcm"
plans = base_path + "/plans/*.dcm"

ctdir     = os.path.expanduser(ctdir)
mcdoses   = os.path.expanduser(mcdoses)
plandoses = os.path.expanduser(plandoses)
mclets    = os.path.expanduser(mclets)
struct    = os.path.expanduser(struct)
plans     = os.path.expanduser(plans)


#os.system("python3 main.py -i {0} -m {1} -l {2} -s {3} -p {4}".format(ctdir,mcdoses,mclets,struct,plans))
print("python3 main.py -i {0} -m {1} -l {2} -s {3}".format(ctdir,mcdoses,mclets,struct))
#os.system("python3 main.py -i {0} -m {1} -l {2} -s {3}".format(ctdir,mcdoses,mclets,struct))