import fractions
import pydicom as pd
import numpy as np
from scipy.spatial import ConvexHull
import code
import copy
import readline											  
import rlcompleter 
import errno
import os
import sys
ERROR_INVALID_NAME = 123


# Check this thread
# https://stackoverflow.com/questions/9532499/check-whether-a-path-is-valid-in-python-without-creating-a-file-at-the-paths-ta
def is_pathname_valid(pathname: str) -> bool:
    '''
    `True` if the passed pathname is a valid pathname for the current OS;
    `False` otherwise.
    '''
    # If this pathname is either not a string or is but is empty, this pathname
    # is invalid.
    try:
        if not isinstance(pathname, str) or not pathname:
            return False

        # Strip this pathname's Windows-specific drive specifier (e.g., `C:\`)
        # if any. Since Windows prohibits path components from containing `:`
        # characters, failing to strip this `:`-suffixed prefix would
        # erroneously invalidate all valid absolute Windows pathnames.
        _, pathname = os.path.splitdrive(pathname)

        # Directory guaranteed to exist. If the current OS is Windows, this is
        # the drive to which Windows was installed (e.g., the "%HOMEDRIVE%"
        # environment variable); else, the typical root directory.
        root_dirname = os.environ.get('HOMEDRIVE', 'C:') \
            if sys.platform == 'win32' else os.path.sep
        assert os.path.isdir(root_dirname)   # ...Murphy and her ironclad Law

        # Append a path separator to this directory if needed.
        root_dirname = root_dirname.rstrip(os.path.sep) + os.path.sep

        # Test whether each path component split from this pathname is valid or
        # not, ignoring non-existent and non-readable path components.
        for pathname_part in pathname.split(os.path.sep):
            try:
                os.lstat(root_dirname + pathname_part)
            # If an OS-specific exception is raised, its error code
            # indicates whether this pathname is valid or not. Unless this
            # is the case, this exception implies an ignorable kernel or
            # filesystem complaint (e.g., path not found or inaccessible).
            #
            # Only the following exceptions indicate invalid pathnames:
            #
            # * Instances of the Windows-specific "WindowsError" class
            #   defining the "winerror" attribute whose value is
            #   "ERROR_INVALID_NAME". Under Windows, "winerror" is more
            #   fine-grained and hence useful than the generic "errno"
            #   attribute. When a too-long pathname is passed, for example,
            #   "errno" is "ENOENT" (i.e., no such file or directory) rather
            #   than "ENAMETOOLONG" (i.e., file name too long).
            # * Instances of the cross-platform "OSError" class defining the
            #   generic "errno" attribute whose value is either:
            #   * Under most POSIX-compatible OSes, "ENAMETOOLONG".
            #   * Under some edge-case OSes (e.g., SunOS, *BSD), "ERANGE".
            except OSError as exc:
                if hasattr(exc, 'winerror'):
                    if exc.winerror == ERROR_INVALID_NAME:
                        return False
                elif exc.errno in {errno.ENAMETOOLONG, errno.ERANGE}:
                    return False
    # If a "TypeError" exception was raised, it almost certainly has the
    # error message "embedded NUL character" indicating an invalid pathname.
    except TypeError as exc:
        return False
    # If no exception was raised, all path components and hence this
    # pathname itself are valid. (Praise be to the curmudgeonly python.)
    else:
        return True
    # If any other exception was raised, this is an unrelated fatal issue
    # (e.g., a bug). Permit this exception to unwind the call stack.
    #
    # Did we mention this should be shipped with Python already?


def is_path_creatable(pathname: str) -> bool:
    '''
    `True` if the current user has sufficient permissions to create the passed
    pathname; `False` otherwise.
    '''
    # Parent directory of the passed path. If empty, we substitute the current
    # working directory (CWD) instead.
    dirname = os.path.dirname(pathname) or os.getcwd()
    return os.access(dirname, os.W_OK)

def is_path_exists_or_creatable(pathname: str) -> bool:
    '''
    `True` if the passed pathname is a valid pathname for the current OS _and_
    either currently exists or is hypothetically creatable; `False` otherwise.

    This function is guaranteed to _never_ raise exceptions.
    '''
    try:
        # To prevent "os" module calls from raising undesirable exceptions on
        # invalid pathnames, is_pathname_valid() is explicitly called first.
        return is_pathname_valid(pathname) and (
            os.path.exists(pathname) or is_path_creatable(pathname))
    # Report failure on non-fatal filesystem complaints (e.g., connection
    # timeouts, permissions issues) implying this path to be inaccessible. All
    # other exceptions are unrelated fatal issues and should not be caught here.
    except OSError:
        return False






def strp(string):
	"""
	Removes all special characters from string and returns the stripped string
	"""
	return ''.join(e for e in string if e.isalnum())

def load_dcmdir(dir,modality):
	"""
	Loads all dicoms of given modality from directory into a list and returns the list of pydicom dicoms.
	"""
	if not dir[-1] == "/":
		dir = dir + "/"
	dcmlist = []
	for file in os.listdir(dir):
		if file[-4:] == ".dcm":
			ds = pd.read_file(dir + file)
			if ds.Modality.lower() == modality.lower():
				dcmlist.append(ds)
	return dcmlist
def load_dcmlist(dcms,modality):
	"""
	Loads all dicoms of given modality from list of paths into a list and returns the list of pydicom dicoms.
	"""
	dcmlist = []
	for path in dcms:
		if path[-4:] == ".dcm":
			ds = pd.read_file(path)
			if ds.Modality.lower() == modality.lower():
				dcmlist.append(ds)
	return dcmlist

def isPointInXyz(pointXyz, Xyz):
	"""
	Checks whether a point (x,y,z) is within a polygon with border points [[xi,yi,zi],[xi+1,yi+1,zi+1]...]
	Returns True if inside, False if outside.
	"""
	# see https://stackoverflow.com/questions/29311682/finding-if-point-is-in-3d-poly-in-python
	poly = Xyz
	point = pointXyz
	hull = ConvexHull(poly)
	new_hull = ConvexHull(np.concatenate((poly, [point])))
	return np.array_equal(new_hull.vertices, hull.vertices)

def isPointInROI(pointXyz, structure, ROI_ID):
	"""
	Checks whether a point (x,y,z) is within a ROI of structure.
	Returns True if inside, False if outside.
	"""
	refROINumber = ROI_ID
	if type(ROI_ID) == str:
		refROINumber = -1
		for seq in structure.StructureSetROISequence:
			if seq.ROIName.lower().strp() == ROI_ID.lower().strp():
				refROINumber = seq.ROINumber
	thisROIContourSequence = getROIContourSequence(structure, refROINumber)
	contourXyz = getROIContourSequenceXyz(structure, refROINumber)

	return isPointInXyz(pointXyz, contourXyz)

def getROIContourSequence(structure, refROINumber):
	"""
	Returns the ROIContourSequence for a given ROI number of the structure given.
	"""
	for seq in structure.ROIContourSequence:	
		if refROINumber == seq.ReferencedROINumber:
			return seq

def getROIContourSequenceXyz(structure, refROINumber):
	"""
	Returns the contour xyz data in format [[xi,yi,zi]] for the given structure ROI number
	"""
	ROIContourSequence = getROIContourSequence(structure, refROINumber)
	xyz = []
	for seq in ROIContourSequence.ContourSequence:
		for j in range(int(len(seq.ContourData)/3)):
			x = seq.ContourData[3*j+0]
			y = seq.ContourData[3*j+1]
			z = seq.ContourData[3*j+2]
			xyz.append([x,y,z])
	return xyz

def getContourDataXyz(contourData):
	"""
	Returns the contour xyz data in format [[xi,yi,zi]] for the given contourData structure
	"""
	n = int(len(contourData)/3)
	xyz = []
	for i in range(n):
		x = contourData[3*i+0]
		y = contourData[3*i+1]
		z = contourData[3*i+2]
		xyz.append([x,y,z])

def read_alphabeta_dict(path):
	"""
	Reads the file in path and returns a dictionary with alpha beta values for each structure in the file.
	Assumes units of Grey
	Assumes something like:
	$ cat path
	"prostate" 2.18
	"Seminal vescicles" 2.5
	"Rectum" 2.2
	Please use the actual structure names found in the RTSTRUCT DICOM.
	Always have a tag for "rest of body". Otherwise it will prompt for it.

	Returns:   
		a dictionary with alpha beta values for each structure in the file.
	"""
	ab_dict = {}
	with open(path,'r') as f:
		for line in f:
			s = line
			start = s.find('"')+1
			end = s.find('"', start)
			word = s[start:end]
			alphabeta = float(line[end:].split()[1].replace("\n",""))
			ab_dict[word] = alphabeta
	if "rest_of_body" not in ab_dict.keys():
		successful = False
		while not successful:
			body = input("Please input the alpha/beta ratio for the rest of the body\n>>> ")
			if body.replace(".","").isnumeric():
				body = float(body)
				successful = True
		ab_dict["rest_of_body"] = body
	return ab_dict

def printStructures(structure):
	print("There are the following structures available in your RTSTRUCT:")
	for i, name in enumerate([a.ROIName for a in structure.StructureSetROISequence]):
		print("{0:2}: {1}".format(i,name))

def prompt_alphabeta_dict(structure, disrgd_bdy = False):
	printStructures(structure)
	def ask():
		print("Please choose the structures you wish to provide alpha/beta ratios for.")
		indices = input("Choose by their number, seperate structures with a space:\n>>> ")
		if len(indices) == 0:
			print("Please choose structures by inputting their numbers. Seperate structures with a space.")
			return ask()
		for ind in indices.split():
			if not ind.isnumeric():
				print("Please only input numbers corresponding to a structue in the given list!")
				return ask()
			if int(ind) >= len(structure.StructureSetROISequence):
				print("Please only input numbers corresponding to a structue in the given list!")
				return ask()
			if int(ind) < 0:
				print("Please only input numbers corresponding to a structue in the given list!")
				return ask()
		
		indices = [int(a) for a in indices.split()]
		return indices
	indices = ask()
	print("Now you must input the alpha/beta ratios for your selected structures.")
	print("Please input them in units of Gy.")
	alphabetas = {}
	for ind in indices:
		stName = [a.ROIName for a in structure.StructureSetROISequence][ind]
		successful = False
		while not successful:
			alpbet = input("Please input the alpha/beta ratio for {0}\n>>> ".format(stName))
			if not alpbet.replace(".","").isnumeric():
				print("Please input a decimal number for the alpha/beta ratio for {0}".format(stName))
				continue
			alphabetas[stName] = float(alpbet)
			successful = True
	successful = False
	while not successful:
		body = input("Please input the alpha/beta ratio for the rest of the body\n>>> ")
		if body.replace(".","").isnumeric():
			body = float(body)
			successful = True
	alphabetas["rest_of_body"] = body
	if not disrgd_bdy:
		successful = False
		while not successful:
			body = input("Please input the index for the structure defining the entire body\n>>> ")
			if body.replace(".","").isnumeric():
				bodyIndex = int(body)
				successful = True
		alphabetas["body_name"] = [a.ROIName for a in structure.StructureSetROISequence][bodyIndex]
	return alphabetas

def make_dose_averaged_let(doses, lets, interactive = False):
	# verify that they have the same length / same amount of LET files and dose files
	if type(doses) == type(lets) == list:
		assert len(doses) == len(lets), "There should be the same amount of dose and LET RTDOSE files"
	# if not a list, only verify that the patient names are the same
	else:
		assert doses.PatientName == lets.PatientName, "Patient names should be equal for LET and dose files"
		return lets
	
	for dos, let  in zip(doses,lets):
		assert dos.PatientName == let.PatientName, "Patient names should be equal for LET and dose files"
		assert dos.Rows == let.Rows, "dose RTDOSE and LET RTDOSE should have same dimensions"
		assert dos.Columns == let.Columns, "dose RTDOSE and LET RTDOSE should have same dimensions"
		assert dos.NumberOfFrames == let.NumberOfFrames, "dose RTDOSE and LET RTDOSE should have same dimensions"

	scratch_rtdose = copy.deepcopy(lets[0])
	scratch_rtdose.SOPInstanceUID = scratch_rtdose.SOPInstanceUID[0:30] + str(np.random.randint(00000,99999)) + scratch_rtdose.SOPInstanceUID[-15:]
	scratch_rtdose.SeriesInstanceUID = scratch_rtdose.SOPInstanceUID[0:54] + str(np.random.randint(00000,99999))

	new_pix_arr = np.zeros_like(scratch_rtdose.pixel_array)
	sum_dose_times_let = np.zeros_like(scratch_rtdose.pixel_array)
	sum_dose = np.zeros_like(scratch_rtdose.pixel_array)
	for dos, let in zip(doses,lets): # calculating dose averaged let: letd = sum_i(dose_i*let_i) / sum_i(dose_i)
		d_arr = dos.pixel_array * dos.DoseGridScaling
		l_arr = let.pixel_array * let.DoseGridScaling
		sum_dose_times_let = sum_dose_times_let + d_arr * l_arr
		sum_dose = sum_dose + d_arr

	sum_dose[np.where(sum_dose_times_let==0)] = 1 # can set to 1 since nominator is 0 anyway (not dividing by 0 this way...)
	this_dose_averaged_let = sum_dose_times_let / sum_dose
	this_dose_averaged_let[np.where(sum_dose_times_let==0)] = 0
	new_pix_arr = this_dose_averaged_let
	
	# Now need to find a good dosegridscaling (dgs), such that max(new_pix_arr) / dgs = 65535 = 2^16-1
	dgs = np.max(new_pix_arr) / (2**16 -1)
	new_pix_arr = new_pix_arr / dgs

	scratch_rtdose.BitsAllocated = 16
	scratch_rtdose.BitsStored = 16
	scratch_rtdose.HighBit = 15
	scratch_rtdose.DoseGridScaling = dgs
	scratch_rtdose.PixelData = new_pix_arr.astype(np.uint16).tobytes()

	if interactive:
		location = "We are in make_dose_averaged_let, helpers.py, line 345"
		vars = globals()       
		vars.update(locals())
		readline.set_completer(rlcompleter.Completer(vars).complete) 
		readline.parse_and_bind("tab: complete")                     
		code.InteractiveConsole(vars).interact()
	#
	# location = "Now in LETd calculator"
	# vars = globals()       
	# vars.update(locals())
	# readline.set_completer(rlcompleter.Completer(vars).complete) 
	# readline.parse_and_bind("tab: complete")                     
	# code.InteractiveConsole(vars).interact()

	return scratch_rtdose


def make_coord_array(rtdose):
	x0,y0,z0 = rtdose.ImagePositionPatient
	dx, dy = rtdose.PixelSpacing
	dz = rtdose.SliceThickness
	if type(dz) != float:
		dz = rtdose.GridFrameOffsetVector[1] - rtdose.GridFrameOffsetVector[0]
	nx = rtdose.Columns
	ny = rtdose.Rows
	nz = rtdose.NumberOfFrames
	p = rtdose.pixel_array
	grid = np.zeros((p.shape[0], p.shape[1], p.shape[2], 3))
	for iz in range(len(grid)):
		z = z0 + iz * dz
		for iy in range(len(grid[iz])):
			y = y0 + dy/2 + iy * dy
			for ix in range(len(grid[iz][iy])):
				x = x0 + dx/2 + ix * dx
				grid[iz][iy][ix][0] = x
				grid[iz][iy][ix][1] = y
				grid[iz][iy][ix][2] = z
	return grid


def ROIName2Number(struct,ROIName):
	return {a.ROIName : a.ROINumber for a in struct.StructureSetROISequence}[ROIName]

def rbe_calculator(dose_list, let_list, alpha_beta_grid, fraction_list, rbemodel, biodose = True, interactivemode=False):
	""" Calculates variable RBE for the given dose grid and returns a new RTDOSE object with variable RBE accounted for.

	Args:
		dose_list (pydicom.dataset.FileDataset): list of Pydicom RTDOSE object with calculated dose in biological (or physical dose) - set biodose accordingly
		let (pydicom.dataset.FileDataset): list of Pydicom RTDOSE object with calculated LET as dose averaged LET in units of kev/um or equivalent
		alpha_beta_grid (numpy.ndarray): Grid of alpha/beta values for every voxel which is in the given dose and let - see linear quadratic model
		fraction_list (int): list of number of fractions for each corresponding dose and let this treatment is supposed to be given in
		rbemodel (string): String describing the RBE model you wish to use.
		biodose (bool, optional): Is the calculated dose given in biological dose (physical * 1.1)? If given in physical dose set to False. Defaults to True.
	"""


	if len(np.unique(fraction_list)) == 1: # if there is only one fractionation scheme present
		pass
	else:
		raise NotImplementedError("It is not yet possible to calculate variable RBE for \
								treatments consisting of multiple fractionation schemes.")


	scratch_rtdose = copy.deepcopy(dose_list[0])
	scratch_rtdose.SOPInstanceUID = scratch_rtdose.SOPInstanceUID[0:30] + str(np.random.randint(00000,99999)) + scratch_rtdose.SOPInstanceUID[-15:]
	scratch_rtdose.SeriesInstanceUID = scratch_rtdose.SOPInstanceUID[0:54] + str(np.random.randint(00000,99999))
	scratch_rtdose.DoseComment = "Variable RBE calculated dose based on {0} RBE model".format(rbemodel)
	scratch_rtdose.SeriesDescription = "Variable RBE calculated dose based on {0} RBE model".format(rbemodel)

	nonsensical_ab = 1234567
	alpha_beta_grid[np.where(alpha_beta_grid == 0)] = nonsensical_ab # we can set these values to be something 
													 # stupid such that we dont divide by 0, and it doesn't matter
													 # because alpha/beta is only 0 outside the body (and thus undefined I guess)
	

 
	# rbe max and min on the form: rbemax = c0 + c1 * c2 * (dose averaged let as ndarray) / (alpha/beta as ndarray)
	rbeminmax 	= lambda c0, c1, c2, l, a: c0 + c1 * c2 * l / a
	rbemin 		= lambda c0, c1, c2, l, a: rbeminmax(c0,c1,c2,l,a)
	rbemax 		= lambda c0, c1, c2, l, a: rbeminmax(c0,c1,c2,l,a)
	## rbe model lambdas. f(phys_dose_per_frac_ndarray, dose_averaged_let_ndarray, alphabeta_ndarray) => rbe_ndarray
	# rbe max and rbe min for carabe (i think): doi: 10.1186/s13014-016-0642-6
	# Wedenberg, see Jesper Pedersen PhD report, or Kristin master thesis, 
	# or read original paper https://doi.org/10.3109/0284186X.2012.705892
	carabe 	  	= lambda d, l, a: (a * (rbemax(0.834,0.154,2.686,l,a)) + np.sqrt( a**2 * (rbemax(0.834,0.154,2.686,l,a))**2 + 4 * d * ( a + d ) * (rbemin(1.09,0.006,2.686,l,a))**2 ) ) / ( 2 * (a + d) )
	wedenberg 	= lambda d, l, a: 1 / (2*d) * (    np.sqrt(      a**2 + 4 * d * a * rbemax(1,1,0.434,l,a) + 4 * d**2 * 1**2          )     -     a     )
	mcnamara    = lambda d, l, a: 1 / (2*d) * (    np.sqrt(      a**2 + 4 * d * a * rbemax(0.999064,1,0.35605,l,a) + 4 * d**2 * np.power((1.1012 - 0.0038709 * np.sqrt(a) * l),2)     )     -     a      )
	lwd         = lambda d, l, a: 1 + 0.055 * l
	rbe_models 	= 	{"carabe"    : carabe,
					 "wedenberg" : wedenberg,
					 "mcnamara"  : mcnamara,
					 "lwd"		 : lwd}
	# And now rbe_models[rbemodel] is a function taking (phys dose per fraction, dose averaged let, alphabeta) and output rbe

	# is the given RBE model in our dictionary?? Otherwise pick one...
	if rbemodel.lower() not in rbe_models.keys():
		print("The RBE model you have requested is not known to this function. Please choose one of ours.")
		rbemodelnr = "None"
		while not rbemodelnr.isnumeric and not int(rbemodelnr) <= len(rbe_models.keys()):
			print("Please choose one from this list.")
			for i,key in enumerate(list(rbe_models.keys())):
				print("{0}: {1}".format(i,key))
			rbemodelnr = input("Choose an index:\n>>> ")
		rbemodel = list(rbe_models.keys())[rbemodelnr]
	
	
	divider = 1.1 if biodose else 1
	dose_grid_list = [dose.pixel_array * dose.DoseGridScaling for dose in dose_list]
	let_grid_list =   [let.pixel_array *  let.DoseGridScaling for let  in let_list]

	# list of physical doses per fraction for each treatment field
	phys_dose_per_frac_grid_list = [dose_grid / divider / fractions for dose_grid, fractions in zip(dose_grid_list, fraction_list)]
	
	# I believe RBE is calculated using all the summed physical doses and the total dose averaged LET
	sum_phys_dose_per_fraction_grid = sum(phys_dose_per_frac_grid_list)
	let_d_total = make_dose_averaged_let(dose_list, let_list)
	let_d_total_grid = let_d_total.pixel_array * let_d_total.DoseGridScaling

	# list of rbe grids for each dose / let combination (for each treatment field)
	# to avoid dividing by 0 in voxels with no dose, we set the dose to be a value in these voxels and then set these RBE voxels to 1.1 afterwards
	sum_phys_dose_per_fraction_grid[np.where(sum_phys_dose_per_fraction_grid==0)] = nonsensical_ab
	rbe_grid = rbe_models[rbemodel](sum_phys_dose_per_fraction_grid, let_d_total_grid, alpha_beta_grid)
	sum_phys_dose_per_fraction_grid[np.where(sum_phys_dose_per_fraction_grid == nonsensical_ab)] = 0
	rbe_grid[np.where(sum_phys_dose_per_fraction_grid==0)] = 1.1
	# setting rbe to 0 where dose is 0
	rbe_grid[sum_phys_dose_per_fraction_grid==0] = 0
	# biological dose per fraction for each treatment field
	sum_bio_dose_per_fraction_grid = rbe_grid * sum_phys_dose_per_fraction_grid
	# biological dose in total for each treatment field
	bio_dose_grid = sum_bio_dose_per_fraction_grid * fraction_list[0]

	# preparing output rtdose 
	dgs = np.max(bio_dose_grid) / (2**16 -1)
	bio_dose_grid = bio_dose_grid / dgs

	scratch_rtdose.BitsAllocated = 16
	scratch_rtdose.BitsStored = 16
	scratch_rtdose.HighBit = 15
	scratch_rtdose.PixelData = bio_dose_grid.astype(np.uint16).tobytes()
	scratch_rtdose.DoseGridScaling = dgs

	return (scratch_rtdose, rbe_grid)

def array2RTDOSE(arr, rtdose_example, seriesdescription):
	# setting up dicom

	assert len(arr.shape) == 3, "Array has to be 3 dimensional (even if matrix-like, then make it (1,n,m) or whatever."

	scratch_rtdose = copy.deepcopy(rtdose_example)
	scratch_rtdose.SOPInstanceUID = scratch_rtdose.SOPInstanceUID[0:30] + str(np.random.randint(00000,99999)) + scratch_rtdose.SOPInstanceUID[-15:]
	scratch_rtdose.SeriesInstanceUID = scratch_rtdose.SOPInstanceUID[0:54] + str(np.random.randint(00000,99999))
	scratch_rtdose.DoseComment = seriesdescription
	scratch_rtdose.SeriesDescription = seriesdescription

	scratch_rtdose.NumberOfFrames = arr.shape[0]
	scratch_rtdose.Rows = arr.shape[1]
	scratch_rtdose.Columns = arr.shape[2]

	dgs = np.max(arr) / (2**16 -1)
	arr = arr / dgs

	scratch_rtdose.BitsAllocated = 16
	scratch_rtdose.BitsStored = 16
	scratch_rtdose.HighBit = 15
	scratch_rtdose.PixelData = arr.astype(np.uint16).tobytes()
	scratch_rtdose.DoseGridScaling = dgs
	return scratch_rtdose
	#

def sum_doses(doselist):
	scratch_rtdose = copy.deepcopy(doselist[0])
	scratch_rtdose.SOPInstanceUID = scratch_rtdose.SOPInstanceUID[0:30] + str(np.random.randint(00000,99999)) + scratch_rtdose.SOPInstanceUID[-15:]
	scratch_rtdose.SeriesInstanceUID = scratch_rtdose.SOPInstanceUID[0:54] + str(np.random.randint(00000,99999))
	scratch_rtdose.DoseComment = "Summed doses"
	sum_dose_arr = np.zeros_like(doselist[0].pixel_array)
	for dose in doselist:
		sum_dose_arr = sum_dose_arr + dose.pixel_array * dose.DoseGridScaling

	

	dgs = np.max(sum_dose_arr) / (2**16 -1)
	sum_dose_arr = sum_dose_arr / dgs

	scratch_rtdose.BitsAllocated = 16
	scratch_rtdose.BitsStored = 16
	scratch_rtdose.HighBit = 15
	scratch_rtdose.DoseGridScaling = dgs
	scratch_rtdose.PixelData = sum_dose_arr.astype(np.uint16).tobytes()

	return scratch_rtdose

def qck_gridplot(grid,slice):
	import matplotlib.pyplot as plt
	plt.figure()
	plt.imshow(grid[slice],cmap='hot')
	plt.show()

def qck_rtdose_plot(rtdose,slice):
	import matplotlib.pyplot as plt
	grid = rtdose.pixel_array * rtdose.DoseGridScaling
	plt.figure()
	plt.imshow(grid[slice],cmap='hot')
	plt.show()

def qck_rtdose_diff_plot(rt0, rt1, slice, scaling = (1,1)):
	grid = rt0.pixel_array*rt0.DoseGridScaling * scaling[0] - rt1.pixel_array*rt1.DoseGridScaling * scaling[1]
	qck_gridplot(grid,slice)




def dumb_rbe_calc(dose,let,alpha,f):
	dose_pf = dose/f
	newgrid = np.zeros(shape=dose_pf.shape)
	for i in range(len(dose_pf)):
		for j in range(len(dose_pf[i])):
			for k in range(len(dose_pf[i][j])):
				d = dose_pf[i][j][k]
				l = let[i][j][k]
				a = alpha[i][j][k]
				mi = 1.09 + 0.006*2.686*l/a
				ma = 0.834+0.154*2.686*l/a
				r = (a*ma + (a*a * ma*ma + 4*d*(a+d)*mi*mi)**0.5) / (2*(a+d))
				newgrid[i][j][k] = r
	return newgrid
