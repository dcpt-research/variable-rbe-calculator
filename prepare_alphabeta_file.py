import argparse
import sys
import pydicom as pd
import code
import readline                                              
import rlcompleter

def code_interact(globals, locals):
	vars = globals
	vars.update(locals)
	readline.set_completer(rlcompleter.Completer(vars).complete)
	readline.parse_and_bind("tab: complete")
	code.InteractiveConsole(vars).interact()	

def parse_var(s):
	"""
	Parse a key, value pair, separated by '='
	That's the reverse of ShellArgs.

	On the command line (argparse) a declaration will typically look like:
		foo=hello
	or
		foo="hello world"
	"""
	items = s.split('=')
	key = items[0].strip() # we remove blanks around keys, as is logical
	if len(items) > 1:
		# rejoin the rest:
		value = '='.join(items[1:])
	return (key, value)


def parse_vars(items):
	"""
	Parse a series of key-value pairs and return a dictionary
	"""
	d = {}

	if items:
		for item in items:
			key, value = parse_var(item)
			d[key] = value
	return d

def printStructures(structure):
		print("There are the following structures available in your RTSTRUCT:")
		for i, name in enumerate([a.ROIName for a in structure.StructureSetROISequence]):
				print("{0:2}: {1}".format(i,name))
def selectStructures(structure, selections):

	return_list = ["NOT_FOUND_YET"] * len(selections)
	# first we try to find the correct structures. Then only ask for what we need.
	listOfStructures = [a.ROIName for a in structure.StructureSetROISequence]
	for i, name in enumerate(listOfStructures):
		if name.lower() in [l.lower() for l in selections]:
			sel_indx = -1
			for j,sel in enumerate(selections):
				if sel.lower() == name.lower():
					sel_indx = j
					break
			if sel_indx != -1:
				return_list[sel_indx] = name
	if "NOT_FOUND_YET" not in return_list:
		return return_list
	
	missing_indices = []
	for i, struct in enumerate(return_list):
		if struct == "NOT_FOUND_YET":
			missing_indices.append(i)
	
	missing_structs = [selections[i] for i in missing_indices]
	printStructures(structure)
	print("Please select structures. Select the structures in the given order:")
	print("If you are unable to find a structure, write \"-1\" in that structures place")
	for organ in missing_structs:
		print("\""+organ+"\"" +" ",end="")
	print("")
	selection=input(">>> ").split(" ")
	selection_ints = []
	for indx in selection:
		if indx == "-1":
			continue
		if indx.isnumeric():
			selection_ints.append(int(indx))
	
	structure_names_chosen = [[a.ROIName for a in structure.StructureSetROISequence][indx] for indx in selection_ints]
	for missing_index, missing_struct in zip(missing_indices, structure_names_chosen):
		return_list[missing_index] = missing_struct
	# now we remove all "NOT_FOUND_YET" entries
	final_return_list = []
	for entry in return_list:
		if entry != "NOT_FOUND_YET":
			final_return_list.append(entry)
	return final_return_list

def main():
	args = parser.parse_args()
	structfile=args.rtstruct_file
	organ_alphabeta_dict = parse_vars(args.set)
	normal_tissue = args.normal_tissue[0]
	outputfile = args.output[0]
	alphabetas = organ_alphabeta_dict.values()
	structure = pd.read_file(structfile)
	structurenames = selectStructures(structure, list(organ_alphabeta_dict.keys()))
	# change these prints to save to file named by -o argument
	with open(outputfile,'w+') as f:
		for organ, alpha in zip(structurenames, alphabetas):
			f.write("\"{0}\" {1}\n".format(organ,alpha))
			#print("\"{0}\" {1}".format(organ,alpha))
		f.write("\"{0}\" {1}\n".format("rest_of_body",normal_tissue))
		#print("\"{0}\" {1}".format("rest_of_body",normal_tissue))
	# here the alphabeta ratio and structures should be processed

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='Tool for creating alpha/beta files for use with variable RBE models. ')
	parser.add_argument("rtstruct_file",type=str,\
				help='Path to the RTstruct file you with to make an alpha/beta ratio file for')	
	parser.add_argument("-o","--output", type=str, nargs=1, required=True,
						help="The file in which to write the alphabeta table")
	parser.add_argument("--normal-tissue", type=float, nargs=1, required=True,
						help="What is the alpha/beta value for normal tissue in Gy")
	parser.add_argument("--set",
						metavar="ORGAN=ALPHABETA", required=True,
						nargs='+',
						help="Set alpha beta values for multiple organs."
							 "(do not put spaces before or after the = sign). "
							 "If a value contains spaces, you should define "
							 "the value as a string with qoutation marks..")
	main()
