usage: main.py [-h] -m DOSE_RTDOSE [DOSE_RTDOSE ...] -l LET_RTDOSE
               [LET_RTDOSE ...] -s STRUCTURES [-o OUTPUT_FILE] [-O OUTPUT_DIR]
               [-a ALPHABETA] [--disregard-body] [--physical-dose]
               [--interactive] --model MODEL --fractions FRACTIONS

Tool for calculating variable RBE models.

options:
  -h, --help            show this help message and exit
  -m DOSE_RTDOSE [DOSE_RTDOSE ...], --dose-rtdose DOSE_RTDOSE [DOSE_RTDOSE ...]
                        List of MC RTDOSE dose files
  -l LET_RTDOSE [LET_RTDOSE ...], --let-rtdose LET_RTDOSE [LET_RTDOSE ...]
                        List of MC RTDOSE LET files
  -s STRUCTURES, --structures STRUCTURES
                        Path to RTSTRUCT file
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        Write the resulting DICOM to this file. Write default
                        to have have default naming.
  -O OUTPUT_DIR, --output-dir OUTPUT_DIR
                        Write the resulting DICOM files to this directory. If
                        it does not exist it will be created
  -a ALPHABETA, --alphabeta ALPHABETA
                        Path to file containing alpha/beta ratios for chosen
                        structures. Names must match the structures in the
                        given RTSTRUCT file.
  --disregard-body      Diseregards the body in terms of alpha beta ratios, as
                        the RBE values are not important outside of the body
                        anyway.
  --physical-dose       Set this if the MC doses are in physical dose.
  --interactive         Set this if you wish to enter interactive mode in code
                        when done. Can be useful for testing.
  --model MODEL         Which RBE model do you wish to use? You can choose
                        "all" for all models.
  --fractions FRACTIONS
                        How many fractions are the treatment designed for?
